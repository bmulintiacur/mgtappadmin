<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\AdminModel;
use Illuminate\Support\Facades\DB;

class DBoperationController extends Controller {
    
    public function dashboard(){
         return view('dashboard');
    }
    

    public function getUsers(){
    	$result = AdminModel::getUsers();
    	return view('users', ['users' => $result]);
    }

    public function getBookings(){
    	$result = AdminModel::getBookings();
    	return view('bookings', ['bookings' => $result]);
    }


}
