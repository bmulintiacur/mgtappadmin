<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminModel extends Model {
	// users list
	public static function getUsers(){
        return DB::table('users')->get();
    }
    // booking data
    public static function getBookings(){
        return DB::table('booking_data')->get();
    }

}