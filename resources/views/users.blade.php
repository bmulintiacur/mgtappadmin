@extends('layouts.app')
@section('title', 'Users')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">@yield('title')</h1>       
    </div>
    <!-- users table -->
     <div class="card shadow mb-4">
        <div class="card-body p-2">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="bg-primary text-white">
                        <tr>
                            <th width="60">Photo</th>
                            <th width="200">First Name</th>
                            <th width="200">Last Name</th>
                            <th width="100">Email</th>
                            <th width="60">Mobile</th>
                            <th width="60">Gender</th>
                            <th width="60">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td class="text-center"><img src="{{'img/user.jpg'}}" id="{{$user->id}}" width="40" height="40" class="rounded"></td>
                            <td id="{{$user->id}}">{{$user->first_name}}</td>
                            <td id="{{$user->id}}">{{$user->last_name}}</td>
		                    <td>{{$user->email_id}}</td>
		                    <td>{{$user->mobile_no}}</td>
		                    <td>{{$user->gender}}</td>
		                    <td>Active</td>
                    	</tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
