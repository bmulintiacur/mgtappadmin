@extends('layouts.app')
@section('title', 'Bookings View')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">@yield('title')</h1>       
    </div>
    <!-- bookind data -->
    <div class="card shadow mb-4">
        <div class="card-body p-2">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="bg-primary text-white">
                        <tr>
               
                            <th width="100">Email</th>
                            <th width="60">Transaction Id</th>
                            <th width="60">Booking Id</th>
                            <th width="60">Total Amount</th>
                            <th width="60">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bookings as $data)
                        <tr>
		                    <td>{{$data->email}}</td>
		                    <td>{{$data->tranaction_id}}</td>
		                    <td>{{$data->booking_id}}</td>
		                    <td>{{$data->total_amount}}</td>
		                    <td>Active</td>
                    	</tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
