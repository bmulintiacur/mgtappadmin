
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
<!--
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
-->
        <div class="sidebar-brand-text"><img src="img/logo.png" width="50%;"> </div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ (Request::path() == 'dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="{{url('/')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ (Request::path()== 'users') ? 'active' : '' }}">
        <a class="nav-link" href="{{url('/users')}}">
            <i class="fas fa-fw fa-blog"></i>
            <span>Users Management</span>
        </a>
    </li>

    <hr class="sidebar-divider my-0">
    <li class="nav-item {{ (Request::path()== 'bookings') ? 'active' : '' }}">
        <a class="nav-link" href="{{url('/bookings')}}">
            <i class="fas fa-envelope-open"></i>
            <span>Bookings View</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->
