<?php

// use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
     if (Auth::check()) {
        return Redirect::to('/dashboard');
    }
    return View::make('login');

});
Route::get('/dashboard', 'DBoperationController@dashboard');
Route::get('/login', function () {
    if (Auth::check()) {
        return Redirect::to('/');
    }
    return View::make('login');
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect('login');
});

Route::post('logincheck', function() {
    ;
    $rules = array(
        'email' => 'required',
        'password' => 'required',
    );
    $v = Validator::make(Input::all(), $rules);
   
    if ($v->fails()) {
        Input::flash();
        return Redirect::to('login')->withInput()->withErrors($v->messages());
    } else {
        $userdata = array(
            'email_id' => Input::get('email'),
            'password' => Input::get('password')
        );
        if (Auth::attempt($userdata)) {
            return Redirect::to('/');
        } else {
            return Redirect::to('login')->withErrors('Incorrect login details');
        }
    }
});

Route::get('/users', 'DBoperationController@getUsers');//->middleware('check_auth');

Route::get('/bookings', 'DBoperationController@getBookings');//->middleware('check_auth');


